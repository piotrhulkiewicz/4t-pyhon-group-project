from datetime import datetime


class Car:
    def __init__(self, model, production_year):
        self.model = model
        self.production_year = production_year
        self.mileage = 0

    def drive(self, distance):
        self.mileage += distance

    def has_warranty(self):
        return True if datetime.now().year - self.production_year <= 7 and self.mileage < 120_000 else False

    def get_description(self):
        return f'This is a {self.model} made in {self.production_year}. Currently it drove {self.mileage} kilometers'


if __name__ == '__main__':
    car1 = Car('Mitsubishi ASX', 2015)
    car2 = Car('Honda Civic', 2020)

    car1.drive(10000)
    car2.drive(125000)
    print('car1 has warranty: ', car1.has_warranty())
    print('car2 has warranty: ', car2.has_warranty())
    print(car1.get_description())
    print(car2.get_description())
