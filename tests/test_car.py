from src.car import Car


def test_new_car_has_0_mileage():
    test_car = Car('Audi A%', 2024)
    assert test_car.mileage == 0


def test_mileage_after_drive_100():
    test_car = Car('Audi A%', 2024)
    test_car.drive(100)
    assert test_car.mileage == 100
    test_car.drive(200)
    assert test_car.mileage == 300


def test_mileage_after_multiple_drive():
    test_car = Car('Audi A%', 2024)
    test_car.drive(100)
    test_car.drive(200)
    assert test_car.mileage == 300


def test_if_2020_model_with_mileage_of_119000_has_warranty():
    test_car = Car('Audi A%', 2020)
    test_car.drive(119000)
    assert test_car.has_warranty()


def test_if_2007_model_with_mileage_of_19000_has_no_warranty():
    test_car = Car('Audi A%', 2007)
    test_car.drive(19000)
    assert not test_car.has_warranty()


def test_if_1998_honda_civic_with_mileage_230000_description_is_correct():
    test_car = Car('Honda Civic', 1998)
    test_car.drive(230_000)
    assert test_car.get_description() == 'This is a Honda Civic made in 1998. Currently it drove 230000 kilometers'
